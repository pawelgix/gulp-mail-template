/**
 * Paweł Gruchociak, pawelgix@gmail.com
 */
 var gulp        = require('gulp');
 var $           = require('gulp-load-plugins')();
 var browserSync = require('browser-sync').create();
 var runSequence = require('gulp4-run-sequence').use(gulp);
 var del         = require('del');
 var sass        = require('gulp-sass')(require('sass'));
 
 var CHARSET_ISO = false;
 var ZIP_NAME = 'mailing.zip';
 
 // clean build dir
 gulp.task('clean', function (cb) {
     return del('build');
     
 });
 
 // Compile sass into CSS & auto-inject into browsers
 gulp.task('scss', function() {
     return gulp.src("scss/*.scss")
         .pipe(sass.sync().on('error', sass.logError))
         .pipe(gulp.dest("css"));
         // .pipe(browserSync.stream());
 });
 
 
 
 
 // Inline styles in HTML & rename source.html to index.html
 // Trigger browser sync reload
 gulp.task('html', function() {
     return gulp.src("source.html")
         .pipe($.inlineCss({
             applyStyleTags: true,
             applyLinkTags: true,
             removeStyleTags: false,
             removeLinkTags: false,
             preserveMediaQueries: true
         }))
         .pipe($.inline({
             base: './',
             // js: uglify,
             // css: minifyCss,
             disabledTypes: ['svg', 'img', 'js'], // Only inline css files
             ignore: ['./css/do-not-inline-me.css']
         }))
         .pipe($.rename('index.html'))
         // .pipe($.convertEncoding({to: 'iso-8859-2'}))
         .pipe(gulp.dest("./"))
         .pipe(browserSync.reload({stream: true}));
 });
 
 
 // Copy files to ./build directory
 gulp.task('copy', function () {
     return gulp.src(['images/**/*.*','index.html'],{base:'.'})
    .pipe(gulp.dest('build'));
 });
 
 
 // Zip build directory
 gulp.task("zip", function() {
     return gulp.src(["build/**/*.*"],{ base:"build" })
     .pipe($.zip(ZIP_NAME))
     .pipe(gulp.dest("./"));
 });
 
 
 // Run build tasks in sequence
 // gulp.task('build', function(cb){
 //     runSequence('clean', 'scss', 'html', 'copy','zip', cb);
 // });
 // Run build tasks in sequence
 gulp.task('build', function (callback) {
     var tasks = ['clean', 'scss', 'html', 'copy'];
     if( CHARSET_ISO ) tasks.push('charset-iso');
     tasks.push('zip');
     tasks.push(callback);
     console.log('Sequence:',tasks);
      runSequence(tasks);
     // callback();
 });
 
 
 
 // Static Server + watching scss/html files
 gulp.task('serve', function(cb) {
 
     browserSync.init({
         server: "./",
         // ghostMode: false,
         // notify:false,
         // proxy: "localhost/gulp/browsersync",
         // open:'local',
         // open:'ui',
     });
 
     gulp.watch(["scss/**/*.scss","*.html"], gulp.series(['build']));//.on('change', browserSync.reload);
 
     cb();
     // gulp.watch("js/*.js").on('change', browserSync.reload);
     // gulp.watch("*.html",['build']);
     // gulp.watch("*.php").on('change', browserSync.reload);
 });
 
 
 
 
 
 
 gulp.task('default', gulp.series(['serve']));
 