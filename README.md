
# README #


```
#!js
npm install && gulp
```


### Gulp tasks
Run live reload server
```
#!js
gulp default
```
ZIP build directory
```
#!js
gulp zip
```
Build & convert from utf-8 to iso-8859-2 
```
#!js
gulp charset-iso
```




### Uses ###

* ZURB Ink
* Gulp (browser-sync, gulp-convert-encoding, gulp-inline, gulp-inline-css, gulp-sass, gulp-zip... )
* SCSS


### License ###

* MIT